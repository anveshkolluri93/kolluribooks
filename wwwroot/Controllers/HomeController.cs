﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace KolluriBooks.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Anvesh"] = "Anvesh's application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewBag.Kolluri = "Anveshkumar's contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
